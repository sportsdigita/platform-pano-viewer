# README #

### What is this repository for? ###

* A jQuery application to set on-top of KRPano. Includes a panorama viewer used to render hotspots and panoramas via JS as well as a hotspot editor.

### Installation ###

*jQuery Required*

##### Using NPM

```bash
npm install git+https://bitbucket.org/sportsdigita/platform-pano-viewer.git --save
```

### Usage ###
```
Include the following files from the src folder in order:
1. utility.js
2. pano-viewer.js
3. pano-editor.js
```

##### Viewer

```javascript
var panoViewer = new PanoViewer();
panoViewer.renderPano(panoData, hotspotData, htmlTargetForPano);
```

##### Editor

```javascript
var panoEditor = new PanoEditor({
   onHotspotSelected: callback,
   onHotspotDeselected: callback,
   onSubHotspotSelected: callback,
   onSubHotspotDeselect: callback,
   onSubHotspotUpdate: callback,
   editButtonImageUrl: string (required),
   defaultContainerImageUrl: string (required)
});

panoEditor.renderEditor(panoData, subhotspotData, htmlTargetForPano);
```