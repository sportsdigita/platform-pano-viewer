var editorListeners = {
    onHotspotSelect: function(instanceId, hotspotName) {
        $.each(this.onHotspotSelectListeners, function(i, data) {
            if (data.id == instanceId)
                data.callback(hotspotName);
        });
    },
    onHotspotSelectListeners: [],

    onSubHotspotSelect: function(instanceId, subHotspotName) {
        $.each(this.onSubHotspotSelectListeners, function(i, data) {
            if (data.id == instanceId)
                data.callback(subHotspotName);
        });
    },
    onSubHotspotSelectListeners: [],

    onDragEnd: function(instanceId, ath, atv) {
        $.each(this.onDragEndListeners, function(i, data) {
            if (data.id == instanceId)
                data.callback(ath, atv);
        });
    },
    onDragEndListeners: []
};

var PanoEditor = function(options) {
    var self = this;
    self.utility = new Utility();
    self.panoViewer = new PanoViewer();

    self.instanceId = self.utility.newGuid();
    self.hotspots = [];
    self.panoDomId = null;

    if (!options.editButtonImageUrl || !options.defaultContainerImageUrl)
        throw new Error("Must include urls for edit button image and default container image.");

    self.options = options || {};

    self.activeHotspot = null;
    self.activeSubHotspot = null;

    self.subHotspotTypes = {
        container: "cont",
        foreground: "fg",
        background: "bg"
    };

    var showContainers = true;
    var showForegrounds = true;
    var showBackgrounds = true;

    // PUBLIC
    self.renderEditor = function(panoData, hotspots, target) {
        if (!panoData)
            throw new Error("Must have pano data to render pano.");
        if (!target)
            throw new Error("Must provide id of div to place pano.");

        self.panoDomId = target;
        var panoDataCopy = $.extend({}, panoData);
        panoDataCopy.includeUrl = null;
        panoDataCopy.previewUrl = null;

        var editButtons = null;
        if (hotspots) {
            $.each(hotspots, function(i, hotspot) {
                self.hotspots.push({
                    original: hotspot,
                    duplicate: JSON.parse(JSON.stringify(hotspot))
                });
            });

            editButtons = createEditButtons(self.hotspots);
        }

        panoDataCopy.styles = [{
            name: "draggable",
            type: "dragging",
            value: "if(pressed, sub(dx,mouse.stagex,drag_adjustx); sub(dy,mouse.stagey,drag_adjusty); screentosphere(dx,dy,ath,atv); if(onDrag!==null, onDrag() ); delayedcall(0,dragging()); , js(editorListeners.onDragEnd(" + self.instanceId + ",get(ath),get(atv))); );"
        }, {
            name: "draggable",
            type: "ondown",
            value: "spheretoscreen(ath,atv,hotspotcenterx,hotspotcentery); sub(drag_adjustx,mouse.stagex,hotspotcenterx); sub(drag_adjusty,mouse.stagey,hotspotcentery); if(onDragStart!==null, onDragStart() ); dragging();"
        }];

        panoDataCopy.renderType = "css3d";
        self.panoViewer.renderPano(panoDataCopy, editButtons || null, target);
    };

    self.setHotspots = function(hotspots) {
        self.hotspots = [];
        $.each(hotspots, function(i, hotspot) {
            var duplicate = JSON.parse(JSON.stringify(hotspot));
            if (!hotspot._id)
                duplicate._id = self.utility.newGuid();

            self.hotspots.push({
                original: hotspot,
                duplicate: duplicate
            });
        });

        var editButtons = createEditButtons(self.hotspots);
        self.panoViewer.setHotspots(editButtons);
    };

    // HOTSPOT
    self.selectHotspot = function(originalHotspot) {
        var hotspot = self.utility.findHotspotByHotspot(originalHotspot, self.hotspots);
        if (hotspot) {
            var duplicateId = hotspot.duplicate._id;
            hotspot.duplicate = JSON.parse(JSON.stringify(hotspot.original));
            hotspot.duplicate._id = duplicateId;
        } else {
            hotspot = {
                original: originalHotspot,
                duplicate: JSON.parse(JSON.stringify(originalHotspot))
            };

            hotspot.duplicate._id = self.utility.newGuid();
            self.hotspots.push(hotspot);
        }

        var subHotspots = [];
        self.activeHotspot = hotspot;

        $.each(hotspot.duplicate.containers, function(i, container) {
            container.name = self.subHotspotTypes.container + "-" + hotspot.duplicate._id + "-" + i;
            container.type = self.subHotspotTypes.container;
            container.visible = showContainers;
            setupSubHotspotForEdit(self.subHotspotTypes.container, container);
            subHotspots.push(container);
        });

        $.each(hotspot.duplicate.backgrounds, function(i, background) {
            background.name = self.subHotspotTypes.background + "-" + hotspot.duplicate._id + "-" + i;
            background.type = self.subHotspotTypes.background;
            background.visible = showBackgrounds;
            setupSubHotspotForEdit(self.subHotspotTypes.background, background);
            subHotspots.push(background);
        });

        $.each(hotspot.duplicate.foregrounds, function(i, foreground) {
            foreground.name = self.subHotspotTypes.foreground + "-" + hotspot.duplicate._id + "-" + i;
            foreground.type = self.subHotspotTypes.foreground;
            foreground.visible = showForegrounds;
            setupSubHotspotForEdit(self.subHotspotTypes.foreground, foreground);
            subHotspots.push(foreground);
        });

        self.panoViewer.setHotspots(subHotspots);

        if (self.options.onHotspotSelected)
            self.options.onHotspotSelected(hotspot.original);
    };

    self.deselectHotspot = function() {
        self.activeHotspot = null;
        self.activeSubHotspot = null;

        var editButtons = createEditButtons(self.hotspots);
        self.panoViewer.setHotspots(editButtons);

        if (self.options.onHotspotDeselect)
            self.options.onHotspotDeselect();
    };
    // END: HOTSPOT


    // SUBHOTSPOT
    self.selectSubHotspot = function(subHotspot) {
        if (self.activeSubHotspot)
            deactivateSubHotspot(self.activeSubHotspot);

        var duplicateSubHotspot = getDuplicateSubHotspot(subHotspot);
        self.activeSubHotspot = duplicateSubHotspot.name;

        self.panoViewer.loadHotspotStyle(self.activeSubHotspot, "draggable");

        var krpanoHotspot = self.panoViewer.getHotspot(self.activeSubHotspot);
        $(krpanoHotspot.sprite).addClass("active");

        $("#" + self.panoDomId).mouseup(function(e) {
            var subHotspotDiv = $(krpanoHotspot.sprite);

            if (!subHotspotDiv.is(e.target) && subHotspotDiv.has(e.target).length === 0) {
                self.deselectSubHotspot();
            }
        });

        if (self.options.onSubHotspotSelected) {
            var nameParts = self.activeSubHotspot.split('-');
            var type = nameParts[0];
            var index = nameParts[2];

            self.options.onSubHotspotSelected(subHotspot, type, index, krpanoHotspot.sprite);
        }
    };

    self.deselectSubHotspot = function() {
        if (!self.activeSubHotspot)
            return;

        deactivateSubHotspot(self.activeSubHotspot);

        if (self.options.onSubHotspotDeselect)
            self.options.onSubHotspotDeselect();
    };

    self.updateSubHotspot = function(subHotspot) {
        if (!self.activeHotspot || !subHotspot)
            return;

        var duplicateSubHotspot = getDuplicateSubHotspot(subHotspot);

        if (!duplicateSubHotspot || !duplicateSubHotspot.name)
            return;

        $.extend(duplicateSubHotspot, subHotspot);

        self.panoViewer.updateSubHotspot(duplicateSubHotspot);

        if (self.options.onSubHotspotUpdate)
            self.options.onSubHotspotUpdate(self.activeHotspot.original);
    };

    self.setSubHotspotTypeVisibility = function(subHotspotType, showSubHotspotType) {
        var subHotspots = [];
        switch (subHotspotType) {
            case self.subHotspotTypes.container:
                showContainers = showSubHotspotType;
                if (self.activeHotspot)
                    subHotspots = self.activeHotspot.duplicate.containers;
                break;
            case self.subHotspotTypes.foreground:
                showForegrounds = showSubHotspotType;
                if (self.activeHotspot)
                    subHotspots = self.activeHotspot.duplicate.foregrounds;
                break;
            case self.subHotspotTypes.background:
                showBackgrounds = showSubHotspotType;
                if (self.activeHotspot)
                    subHotspots = self.activeHotspot.duplicate.backgrounds;
                break;
        };

        $.each(subHotspots, function(i, subHotspot) {
            subHotspot.visible = showSubHotspotType;
            self.panoViewer.updateSubHotspot(subHotspot);
        });
    };

    self.getSubHotspotTypeVisibility = function(subHotspotType) {
        switch (subHotspotType) {
            case self.subHotspotTypes.container:
                return showContainers;
            case self.subHotspotTypes.foreground:
                return showForegrounds;
            case self.subHotspotTypes.background:
                return showBackgrounds;
        }
    };

    // END: SUBHOTSPOT

    // ZOOM LOCATION
    self.lookTo = function(hlookat, vlookat, fov) {
        self.panoViewer.lookTo(hlookat, vlookat, fov)
    };

    self.getCurrentZoomLocation = function() {
        return self.panoViewer.getCurrentZoomLocation();
    };

    self.setZoomLocation = function() {
        if (!self.activeHotspot)
            return;

        var zoomLocation = self.panoViewer.getCurrentZoomLocation();

        self.activeHotspot.original.zoom = {
            ath: zoomLocation.hlookat,
            atv: zoomLocation.vlookat,
            fov: zoomLocation.fov
        };
        self.activeHotspot.duplicate.zoom = {
            ath: zoomLocation.hlookat,
            atv: zoomLocation.vlookat,
            fov: zoomLocation.fov
        };
    };
    // END: ZOOM LOCATION

    // LISTENERS
    var onHotspotSelect = function(hotspotName) {
        var nameParts = hotspotName.split('-');
        var hotspotId = nameParts[1];

        var selectedHotspot = self.utility.findHotspotById(hotspotId, self.hotspots);

        self.selectHotspot(selectedHotspot.original);
    };
    editorListeners.onHotspotSelectListeners.push({ callback: onHotspotSelect, id: self.instanceId });

    var onSubHotspotSelect = function(hotspotName) {
        var subHotspot = getSubHotspotData(hotspotName, true);

        self.selectSubHotspot(subHotspot);
    };
    editorListeners.onSubHotspotSelectListeners.push({ callback: onSubHotspotSelect, id: self.instanceId });

    var onDragEnd = function(ath, atv) {
        var originalSubHotspot = getSubHotspotData(self.activeSubHotspot, true);
        originalSubHotspot.ath = ath;
        originalSubHotspot.atv = atv;
        var duplicateSubHotspot = getSubHotspotData(self.activeSubHotspot, false);
        duplicateSubHotspot.ath = ath;
        duplicateSubHotspot.atv = atv;

        if (self.options.onSubHotspotUpdate)
            self.options.onSubHotspotUpdate(self.activeHotspot.original);
    };
    editorListeners.onDragEndListeners.push({ callback: onDragEnd, id: self.instanceId });
    // END: LISTENERS


    // PRIVATE
    var createEditButtons = function(hotspots) {
        var editButtons = [];

        $.each(hotspots, function(i, hotspot) {
            var hotspotCopy = hotspot.duplicate;
            var postionHotspot = null;

            if (hotspotCopy.containers && hotspotCopy.containers[0]) {
                postionHotspot = hotspotCopy.containers[0];
            } else if (hotspotCopy.backgrounds && hotspotCopy.backgrounds[0]) {
                postionHotspot = hotspotCopy.backgrounds[0];
            } else if (hotspotCopy.foregrounds && hotspotCopy.foregrounds[0]) {
                postionHotspot = hotspotCopy.foregrounds[0];
            }

            if (postionHotspot) {
                var editButton = {
                    name: "edit-" + hotspotCopy._id,
                    url: self.options.editButtonImageUrl,
                    ath: postionHotspot.ath,
                    atv: postionHotspot.atv,
                    width: "32",
                    height: "32",
                    capture: true,
                    handcursor: true,
                    distorted: "false"
                };
                editButton.onclick = "js(editorListeners.onHotspotSelect(" + self.instanceId + ",'" + editButton.name + "') );";

                editButtons.push(editButton);
            }
        });

        return editButtons;
    };

    var setupSubHotspotForEdit = function(hotspotType, hotspot) {
        hotspot.capture = true;
        hotspot.handcursor = true;
        hotspot.bgColor = null;
        hotspot.onclick = "js(editorListeners.onSubHotspotSelect(" + self.instanceId + ",'" + hotspot.name + "') );";

        if (hotspotType == self.subHotspotTypes.container) {
            hotspot.url = self.options.defaultContainerImageUrl;
        } else {
            hotspot.class = "hotspot-border";
        }
    };

    var deactivateSubHotspot = function(subHotspotName) {
        var krpanoHotspot = self.panoViewer.getHotspot(subHotspotName);

        if (krpanoHotspot) {
            $(krpanoHotspot.sprite).removeClass('active');
            self.panoViewer.editHotspotAttribute(subHotspotName, "dragging", "");
            self.panoViewer.editHotspotAttribute(subHotspotName, "ondown", "");
        }

        $("#" + self.panoDomId).off('mouseup');
        self.activeSubHotspot = null;
    };

    var getSubHotspotData = function(hotspotName, getOriginal) {
        var nameParts = hotspotName.split('-');
        var hotspotType = nameParts[0];
        var hotspotId = nameParts[1];
        var index = nameParts[2];

        var selectedDigiDeckHotspot = self.utility.findHotspotById(hotspotId, self.hotspots);
        selectedDigiDeckHotspot = getOriginal ? selectedDigiDeckHotspot.original : selectedDigiDeckHotspot.duplicate;

        if (hotspotType == self.subHotspotTypes.container) {
            return selectedDigiDeckHotspot.containers[index];
        } else
        if (hotspotType == self.subHotspotTypes.foreground) {
            return selectedDigiDeckHotspot.foregrounds[index];
        } else if (hotspotType == self.subHotspotTypes.background) {
            return selectedDigiDeckHotspot.backgrounds[index];
        }
    };

    var getDuplicateSubHotspot = function(subHotspot) {
        var updatedSubHotspot = null;

        $.each(self.activeHotspot.original.containers, function(i, container) {
            if (container == subHotspot)
                updatedSubHotspot = self.activeHotspot.duplicate.containers[i];
        });
        if (updatedSubHotspot)
            return updatedSubHotspot;

        $.each(self.activeHotspot.original.backgrounds, function(i, background) {
            if (background == subHotspot)
                updatedSubHotspot = self.activeHotspot.duplicate.backgrounds[i];
        });
        if (updatedSubHotspot)
            return updatedSubHotspot;

        $.each(self.activeHotspot.original.foregrounds, function(i, foreground) {
            if (foreground == subHotspot)
                updatedSubHotspot = self.activeHotspot.duplicate.foregrounds[i];
        });
        if (updatedSubHotspot)
            return updatedSubHotspot;
    };
};