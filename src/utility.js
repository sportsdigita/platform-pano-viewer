var Utility = function() {

    this.convertDataToUrl = function(dataUrl) {
        var blobBin = atob(dataUrl.split(',')[1]);
        var mimeString = dataUrl.split(',')[0].split(':')[1].split(';')[0];
        var array = [];

        for (var i = 0; i < blobBin.length; i++) {
            array.push(blobBin.charCodeAt(i));
        }
        var blob = new Blob([new Uint8Array(array)], {
            type: mimeString
        });

        return (window.URL ? URL : webkitURL).createObjectURL(blob);
    };

    this.replicateImage = function(repeats, image, container, minimumPadding, repeatHorizontally) {
        var canvas = document.createElement('canvas');
        canvas.height = container.height;
        canvas.width = container.width;
        var context = canvas.getContext("2d");

        var aspectRatio = (image.width / image.height);
        var padding = 0;

        if (repeatHorizontally) {
            var imgWidth = aspectRatio * container.height;
            var widthOfImages = repeats * (imgWidth + minimumPadding * 2);
            padding = (container.width - widthOfImages) / (repeats * 2) + minimumPadding;

            for (var i = 0; i < repeats; i++) {
                var x = (imgWidth * i) + (padding * (i * 2) + padding);
                context.drawImage(image, x, 0, imgWidth, canvas.height);
            }
        } else {
            var imgHeight = container.width / aspectRatio;
            var heightOfImages = repeats * (imgHeight + minimumPadding * 2);
            padding = (container.height - heightOfImages) / (repeats * 2) + minimumPadding;

            for (var i = 0; i < repeats; i++) {
                var y = (imgHeight * i) + (padding * (i * 2) + padding);
                context.drawImage(image, 0, y, container.width, imgHeight);
            }
        }

        return this.convertDataToUrl(canvas.toDataURL());
    };

    this.newGuid = function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + s4() + s4() +
            s4() + s4() + s4() + s4();
    };

    this.findHotspotById = function(id, hotspots) {
        for (var i = 0, len = hotspots.length; i < len; i++) {
            if (id == hotspots[i].duplicate._id)
                return hotspots[i];
        }
        return null;
    };

    this.findHotspotByHotspot = function(hotspot, hotspots) {
        for (var i = 0, len = hotspots.length; i < len; i++) {
            if (hotspot == hotspots[i].original)
                return hotspots[i];
        }
        return null;
    };

};