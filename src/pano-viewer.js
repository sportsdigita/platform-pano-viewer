var PanoViewer = function() {
    var self = this;
    self.utility = new Utility();
    self.panoData = null;
    self.panoId = null;
    self.hotspots = null;
    self.krpano = null;
    var isReady = false;

    self.renderPano = function(panoData, hotspots, target) {
        if (!panoData)
            throw new Error("Must have pano data to render pano.");
        if (!target)
            throw new Error("Must provide id of div to place pano.");

        self.panoData = panoData;
        self.hotspots = hotspots;
        self.panoId = "pano-" + panoData._id;

        var settings = {};

        settings["view.hlookat"] = self.panoData.viewHlookat || 0;
        settings["view.vlookat"] = self.panoData.viewVlookat || 0;
        settings["view.vlookatmin"] = self.panoData.vlookatmin;
        settings["view.vlookatmax"] = self.panoData.vlookatmax;
        settings["view.fov"] = self.panoData.viewFov || 90;
        settings["view.maxpixelzoom"] = self.panoData.viewMaxPixelZoom || 1.0;
        settings["view.fovmax"] = self.panoData.viewFovMax || 90;
        settings["view.limitview"] = self.panoData.viewLimitView || "auto";
        settings["preview.url"] = self.panoData.previewUrl;

        if (self.panoData.imageLevels) {
            // multi-res pano
            settings["image.type"] = self.panoData.imageType;
            settings["image.multires"] = self.panoData.imageMultires;
            settings["image.tilesize"] = self.panoData.imageTileSize;

            $.each(self.panoData.imageLevels, function(i, level) {
                settings["image.level[" + i + "].tiledimagewidth"] = level.tiledImageWidth;
                settings["image.level[" + i + "].tiledimageheight"] = level.tiledImageHeight;
                settings["image.level[" + i + "].cube.url"] = level.cubeUrl;
            });
        } else {
            // single-res pano
            settings["image.cube.url"] = self.panoData.cubeUrl;
        }

        $.each(self.panoData.styles, function(i, style) {
            settings["style[" + style.name + "]." + style.type] = style.value;
        });

        var pano = {
            xml: self.panoData.includeUrl || null,
            id: self.panoId,
            target: target,
            html5: "only" + (self.panoData.renderType ? "+" + self.panoData.renderType : ""),
            vars: settings,
            onready: initializePano
        };

        embedpano(pano);
    };

    self.removePano = function(_id) {
        var panoId = "pano-" + _id;
        removepano(panoId);
    };

    self.setHotspots = function(hotspots) {
        if (isReady) {
            removeAllHotspots();
            self.hotspots = hotspots;

            $.each(self.hotspots, function(i, hotspot) {
                generateHotspot(hotspot);
            });
        }
    };

    self.updateSubHotspot = function(subHotspot) {
        if (!subHotspot || !subHotspot.name)
            throw new Error("Must provide a subhotspot with a name.");

        self.krpano.call("set(hotspot[" + subHotspot.name + "].ath," + subHotspot.ath + ");");
        self.krpano.call("set(hotspot[" + subHotspot.name + "].atv," + subHotspot.atv + ");");
        self.krpano.call("set(hotspot[" + subHotspot.name + "].height," + subHotspot.height + ");");
        self.krpano.call("set(hotspot[" + subHotspot.name + "].width," + subHotspot.width + ");");
        self.krpano.call("set(hotspot[" + subHotspot.name + "].distorted," + (subHotspot.distorted || true) + ");");
        self.krpano.call("set(hotspot[" + subHotspot.name + "].rx," + (subHotspot.rx || 0) + ");");
        self.krpano.call("set(hotspot[" + subHotspot.name + "].ry," + (subHotspot.ry || 0) + ");");
        self.krpano.call("set(hotspot[" + subHotspot.name + "].rz," + (subHotspot.rz || 0) + ");");
        if (subHotspot.scale)
            self.krpano.call("set(hotspot[" + subHotspot.name + "].scale," + subHotspot.scale + ");");
        if (subHotspot.zorder)
            self.krpano.call("set(hotspot[" + subHotspot.name + "].zorder," + subHotspot.zorder + ");");
        if (subHotspot.alpha)
            self.krpano.call("set(hotspot[" + subHotspot.name + "].alpha," + subHotspot.alpha + ");");
        if (typeof subHotspot.visible != "undefined")
            self.krpano.call("set(hotspot[" + subHotspot.name + "].visible," + subHotspot.visible + ");");
    };

    self.addSubHotspot = function(hotspot) {
        generateHotspot(hotspot);
    };

    self.getHotspot = function(hotspotName) {
        return self.krpano.get("hotspot[" + hotspotName + "]");
    };

    self.editHotspotAttribute = function(hotspotName, attr, value) {
        self.krpano.call("set(hotspot[" + hotspotName + "]." + attr + "," + value + ");");
    };

    self.deleteSubHotspot = function(krpanoHotspotName) {
        self.krpano.call("removehotspot(" + krpanoHotspotName + ")");
    };

    self.loadHotspotStyle = function(hotspotName, styleName) {
        self.krpano.call("hotspot[" + hotspotName + "].loadstyle('" + styleName + "');");
    };

    self.lookTo = function(hlookat, vlookat, fov) {
        self.krpano.call("lookto(" + hlookat + "," + vlookat + "," + fov + ");");
    };

    self.getCurrentZoomLocation = function() {
        return {
            hlookat: self.krpano.get("view.hlookat"),
            vlookat: self.krpano.get("view.vlookat"),
            fov: self.krpano.get("view.fov"),
        };
    };

    var initializePano = function(krpano) {
        self.krpano = krpano;
        isReady = true;

        if (!self.hotspots)
            return;

        $.each(self.hotspots, function(i, hotspot) {
            generateHotspot(hotspot);
        });
    };

    var generateHotspot = function(hotspot) {
        if (hotspot.bgColor)
            createBackgroundHotspot(hotspot);

        if (hotspot.repeat && !hotspot.videoUrl) {
            var padding = hotspot.padding || 5;
            hotspot.height -= padding;
            hotspot.width -= padding;

            var img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.onload = function() {
                var numberOfRepeats = 0;
                var imageRatio = this.width / this.height;
                var hsRatio = hotspot.width / hotspot.height;
                var repeatHorizontally = imageRatio < hsRatio;

                if (repeatHorizontally) {
                    var newImageWidth = imageRatio * hotspot.height;
                    numberOfRepeats = Math.floor(hotspot.width / (newImageWidth + 10)); //10 = minPad * 2

                    if (numberOfRepeats > 1) {
                        if (hotspot.maxRepeatVertical < numberOfRepeats)
                            numberOfRepeats = hotspot.maxRepeatVertical;
                        hotspot.url = self.utility.replicateImage(numberOfRepeats, this, { height: hotspot.height, width: hotspot.width }, padding, repeatHorizontally);
                    }

                    hotspot.width = "prop";
                } else {
                    var newImageHeight = hotspot.width / imageRatio;
                    numberOfRepeats = Math.floor(hotspot.height / (newImageHeight + 10)); //10 = minPad * 2

                    if (numberOfRepeats > 1) {
                        if (hotspot.maxRepeatHorizontal < numberOfRepeats)
                            numberOfRepeats = hotspot.maxRepeatHorizontal;
                        hotspot.url = self.utility.replicateImage(numberOfRepeats, this, { height: hotspot.height, width: hotspot.width }, padding, repeatHorizontally);
                    }

                    hotspot.height = "prop";
                }
                generateKrpanoHotspot(hotspot);
            };
            img.src = hotspot.url;
        } else {
            generateKrpanoHotspot(hotspot);
        }
    };

    var createBackgroundHotspot = function(hotspot) {
        var backgroundHotspot = {
            bgColor: hotspot.bgColor,
            ath: hotspot.ath,
            atv: hotspot.atv,
            height: hotspot.height,
            width: hotspot.width,
            rx: hotspot.rx,
            ry: hotspot.ry,
            rz: hotspot.rz
        };
        if (hotspot.scale)
            backgroundHotspot.scale = hotspot.scale;
        backgroundHotspot.zorder = hotspot.zorder ? hotspot.zorder - 1 : 0;

        var canvas = document.createElement('canvas');
        canvas.height = backgroundHotspot.height;
        canvas.width = backgroundHotspot.width;
        var context = canvas.getContext("2d");

        context.fillStyle = backgroundHotspot.bgColor;
        context.fillRect(0, 0, canvas.width, canvas.height);

        backgroundHotspot.url = self.utility.convertDataToUrl(canvas.toDataURL());

        generateKrpanoHotspot(backgroundHotspot);
    };

    var removeAllHotspots = function() {
        self.hotspots = [];
        var allHotspotsCopy = jQuery.extend({}, self.krpano.get('hotspot').getArray());

        $.each(allHotspotsCopy, function(i, hotspot) {
            self.krpano.call("removehotspot(" + hotspot.name + ")");
        });
    };

    var generateKrpanoHotspot = function(hotspot) {
        var clickableByDefault = false; 

        if (!hotspot.name)
            hotspot.name = "hs-" + self.utility.newGuid();

        self.krpano.call("addhotspot(" + hotspot.name + ");");

        if (hotspot.class) {
            var createdKrpanoHotspot = self.krpano.get("hotspot[" + hotspot.name + "]");
            $(createdKrpanoHotspot.sprite).addClass(hotspot.class);
        }

        self.krpano.call("set(hotspot[" + hotspot.name + "].url," + hotspot.url + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].ath," + hotspot.ath + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].atv," + hotspot.atv + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].height," + hotspot.height + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].width," + hotspot.width + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].distorted," + (hotspot.distorted || true) + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].rx," + (hotspot.rx || 0) + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].ry," + (hotspot.ry || 0) + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].rz," + (hotspot.rz || 0) + ");");
        if (hotspot.scale)
            self.krpano.call("set(hotspot[" + hotspot.name + "].scale," + hotspot.scale + ");");
        if (hotspot.zorder)
            self.krpano.call("set(hotspot[" + hotspot.name + "].zorder," + hotspot.zorder + ");");
        if (hotspot.alpha)
            self.krpano.call("set(hotspot[" + hotspot.name + "].alpha," + hotspot.alpha + ");");
        if (typeof hotspot.visible != "undefined")
            self.krpano.call("set(hotspot[" + hotspot.name + "].visible," + hotspot.visible + ");");

        if (hotspot.videoUrl) {
            clickableByDefault = true;
            self.krpano.call("set(hotspot[" + hotspot.name + "].videourl," + hotspot.videoUrl + ");");
            self.krpano.call("set(hotspot[" + hotspot.name + "].onclick," + (hotspot.onClick || "togglePause();") + ");");
            self.krpano.call("set(hotspot[" + hotspot.name + "].pausedonstart," + (hotspot.pauseOnStart || "true") + ");");
            self.krpano.call("set(hotspot[" + hotspot.name + "].onvideocomplete," + (hotspot.onVideoComplete || "stop();") + ");");
            hotspot.onClick = hotspot.onClick || "togglePause();";

            if (hotspot.posterUrl) {
                var posterHotspot = jQuery.extend(true, {}, hotspot);

                posterHotspot.url = hotspot.posterUrl;
                posterHotspot.zorder = hotspot.zorder ? hotspot.zorder + 1 : 50;
                posterHotspot.capture = true;
                posterHotspot.handcursor = true;
                posterHotspot.onclick = "tween(visible,false);hotspot[" + hotspot.name + "].play();";
                delete posterHotspot.videoUrl;
                delete posterHotspot.posterUrl;

                generateKrpanoHotspot(posterHotspot);
            }
        }

        self.krpano.call("set(hotspot[" + hotspot.name + "].capture," + (hotspot.capture || clickableByDefault) + ");");
        self.krpano.call("set(hotspot[" + hotspot.name + "].handcursor," + (hotspot.handcursor || clickableByDefault) + ");");
        if (hotspot.onclick)
            self.krpano.call("set(hotspot[" + hotspot.name + "].onclick," + hotspot.onclick + ");");
    };

};