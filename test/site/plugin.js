$(function() {
    var viewingEditor = false;
    var panoViewer = new PanoViewer();
    var panoEditor = new PanoEditor({
        editButtonImageUrl: "images/edit-button.png",
        defaultContainerImageUrl: "images/transp.png",
        onSubHotspotSelected: function(subhotspot, subhotspotType) {
            $('#subhotspotEditor').show();

            $('#rotateXrange').val(subhotspot.rx);
            $('#rotateYrange').val(subhotspot.ry);
            $('#rotateZrange').val(subhotspot.rz);

            $('#rotateXrange').on('input', function() {
                subhotspot.rx = this.value;
                panoEditor.updateSubHotspot(subhotspot);
            });
            $('#rotateYrange').on('input', function() {
                subhotspot.ry = this.value;
                panoEditor.updateSubHotspot(subhotspot);
            });
            $('#rotateZrange').on('input', function() {
                subhotspot.rz = this.value;
                panoEditor.updateSubHotspot(subhotspot);
            });
        },
        onSubHotspotDeselect: function() {
            $('#rotateXrange').off('input');
            $('#rotateYrange').off('input');
            $('#rotateZrange').off('input');

            $('#subhotspotEditor').hide();
        }
    });

    $("#toggle-editor").click(function() {
        viewingEditor = !viewingEditor;


        panoViewer.removePano(panoData._id);

        viewingEditor ? $(this).text('Viewer') : $(this).text('Editor');
        viewingEditor ? renderPanoEditor() : renderPanoViewer();
    });

    var panoData = {
        _id: "123",
        title: "",
        organizationId: "",
        subdomain: "",
        renderType: "css3d",
        viewHlookat: 0,
        viewVlookat: 20,
        viewFov: 90,
        viewMaxPixelZoom: "1.0",
        viewFovMax: 91,
        viewLimitView: "auto",
        previewUrl: "test.tiles/preview.jpg",
        cubeUrl: "test.tiles/pano_%s.jpg"
    };

    var viewHotspot = {
        "ath": "110.145095",
        "atv": "-24.535842",
        "distorted": "true",
        "url": "images/logo.png",
        "bgColor": "white",
        "zorder": "5",
        "rx": "-23.5",
        "ry": "-8.25",
        "rz": "-0.336817",
        "width": "586",
        "height": "340",
        "scale": "0.221962",
        "depth": "1000"
    };

    var editHotspot = {
        "_id": "58c30b3a947cb6be352c9381",
        "title": "Main Video Board",
        "panoramaId": "52e2e77398fdcaf3218b4579",
        "zoom": {
            "ath": "110",
            "atv": "-24.5",
            "fov": "33.76"
        },
        "containers": [{
            "ath": "110.145095",
            "atv": "-24.535842",
            "distorted": "true",
            "zorder": "5",
            "rx": "-23.5",
            "ry": "-8.25",
            "rz": "-0.336817",
            "width": "586",
            "height": "340",
            "scale": "0.221962",
            "depth": "1000"
        }],
        "backgrounds": [],
        "foregrounds": []
    };

    var renderPanoViewer = function() {
        panoViewer.renderPano(panoData, [viewHotspot], "pano-container");
    };

    var renderPanoEditor = function() {
        panoEditor.renderEditor(panoData, [editHotspot], "pano-container");
    };

    renderPanoViewer();
});